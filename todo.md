# WIP
- Note stuff needs wrapping of data, there are dependencies in score_lib to accessors...
- score_lib in turn needs data extracted in a neat way


# Different instruments
- I think in the end the best way to work it is different soundfonts
- For that to work we'd have to be able to either merge several midi files
	into the same mp3 using different soundfonts OR play several mp3 files
	at once for bouncedown / merge mp3s on same time track.
- Splitting midi by track is easy but using many soundfonts for fluidsynth
	is ungooglable.
- There is of course also the idea of calling fs directly. 
	https://github.com/nwhitehead/pyfluidsynth
	- Say you have a fluidsynth player that plays a list of tracks based
	on a soundfont path. 
	- Given that the conductor only handles OPERATIONS, and the DATA
	is in tracks themselves, you can easily define instruments at the very end.
- then again of COURSE you can merge mp3
	https://stackoverflow.com/questions/14498539/how-to-overlay-downmix-two-audio-files-using-ffmpeg
	- However there's always the issue with no pan control and 
	dirty mp3 over clean midi
	- .. actually panning probably works via midi anyway
- So ideally you would:
	1. Create separate midi files for each track (this is doable using
	existing midifile maker). 
	2. Use https://pypi.org/project/midi2audio/ to call mp3 conversion
	in python for each file with designated soundfont 
	3. Use some kind of python module, like pydub, to overlay/mix the 
	mp3 files
- This could technically all be done in a "mixdown" lib. 
	- Take objects of FONT,TRACK,NAME
	-> Loop, doing the above
-> Work has started on MixDown class
