# Snake Basket
- WIP python library for composing MIDI-works using code only
- See src/main.py for the core example 
    -> "compile" this to mp3 and listen right away with play_main.sh

## Prerequisites
- python (for creating midi files with the source code)
- fluidsynth (for playing midi files or turning them into mp3 with the help of soundfonts)
- soundfonts (sf2 format supported)

## Soundfont resources
- https://rkhive.com/synth.html1
- https://archive.org/download/sf2soundfonts
