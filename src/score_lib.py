# Python 3.7 legacy, can be removed in 4.0
from __future__ import annotations

from note_lib import Note, NoteData
from note_string_lib import NoteParser
import random
import math
from itertools import cycle

def log(message):
    print(message)

def log_title(message):
    print(message)

class Score:

    def __init__(self, name="DefaultScore"):
        self.wipe()
        self.name = name

    def wipe(self) -> type(self):
        self.notes = []
        self.latest_note_data = NoteData(None, 1,1,1)
        self.saved_sections = {}
        return self        

    def get_tones(self) -> list:
        tones = []
        for n in self.notes:
            tones.append(n.tone)
        return tones
    
    def get_durations(self) -> list:
        durations = []
        for n in self.notes:
            data = n.get_NoteData()
            durations.append(data.duration)
        return durations

    def dur(self) -> float:
        dur = 0
        durs = self.get_durations()
        for d in durs:
            dur += d
        return dur

    def get_sustains(self) -> list:
        sustains = []
        for n in self.notes:
            sustains.append(n.sustain)
        return sustains

    def get_amps(self) -> list:
        amps = []
        for n in self.notes:
            amps.append(n.amp)
        return amps

    ### State manipulations ####################################

    # "Pretend the last note had"; useful for assuming properties on subsequent additions
    def start_with(self, dur: float =None, sus: float=None, amp: float=None) -> type(self):

        if dur != None : self.latest_note_data.duration = dur 
        if sus != None : self.latest_note_data.sustain = sus 
        if amp != None : self.latest_note_data.amp = amp

        return self

    ### Utility builder functions #############################

    # Replace tone at index with tone
    def replace(self, index_target: int, replacement_tone: int) -> type(self):
        self.notes[index_target].tone = replacement_tone
        return self

    # Give the last n (or default: all) notes pattern specified a name
    # It can then be referenced for repetition without manually specifying range later
    def save(self, section_name: str, onwards_from: str=None) -> type(self):

        self.saved_sections[section_name] = []
        to_be_cloned = self.notes

        if onwards_from != None:
            # Note that the range part is difficult
            # You must give a "-2" or suchlike to say how many backwards
            to_be_cloned = self.notes[onwards_from:]

        for note in to_be_cloned:
            self.saved_sections[section_name].append(note.clone())

        return self

    def transpose_all(self, amount: int) -> type(self):
        for note in self.notes:
            data = note.get_NoteData()
            data.tone += amount
        return self

    # Attempt at "sloppy fingers" random readjustment
    # Should be done as a final step and thus does not act as a "builder"
    def humanize_old(self, amount: float=0.05):
        sus_adjust = amount
        for _ in range(0, len(self.notes)):
            rand_note:Note = random.choice(self.notes)
            rand_note.get_NoteData().sustain += sus_adjust
            rand_note.get_NoteData().amp += (sus_adjust / 2)
            rand_note.get_NoteData().duration += (sus_adjust / 2)
            sus_adjust = sus_adjust * -1 
        return self   

    def humanize(self, amount: float=0.01):
        sus_adjust = amount
        scatter_max = 0.0001
        ### Make an array of half -1, half 1 with same length as notes
        total_notes = 1
        # "Silent" notes are just placeholders in the midi lib
        # They should not be tampered with
        non_silent_notes = []
        for note in self.notes:
            if (note.get_NoteData().amp != 0):
                if note in non_silent_notes:
                    print("ERROR: Found duplicate note")
                    exit(1)
                non_silent_notes.append(note)
                total_notes += 1

        half_notes = int(total_notes / 2)
        positives = []
        for _ in range(0, half_notes):
            positives.append(1)
        negatives = []
        for _ in range(0, half_notes):
            negatives.append(-1)

        # Combine them and shuffle the resulting array (random order, 50% distribution)
        adjustments = positives + negatives
        random.shuffle(adjustments)
        print(adjustments)

        # Loop all notes and apply shift in duration according to -1 or 1 multiplier (end result: same total duration)
        index = 0
        for note in non_silent_notes:
            adjustment = adjustments[index]
            note.get_NoteData().sustain += (adjustment * (sus_adjust + random.uniform(0, scatter_max)))
            
            old = str(note.get_NoteData().duration)
            note.get_NoteData().duration += (adjustment * (sus_adjust + random.uniform(0, scatter_max) / 2))
            #print("DEBUG: note dur " + old + ":" + str(note.get_NoteData().duration))
            note.get_NoteData().amp += (adjustment * (sus_adjust))
            index += 1
        return self 

    def reverse(self):
        self.notes.reverse()
        return self

    def clone(self) -> Score:
        clone = Score()
        for note in self.notes:
            cast_note : Note = note
            clone._add_note(cast_note.clone())
        return clone 

    ### Note manipulation; functions that add Notes ###########

    def parse(self, string: str, amp=1, base_dur=1/2, sus_x=0.9) -> type(self):
        parser = NoteParser(string, amp, base_dur, sus_x)
        for note in parser.note_list:
            self._add_note(note)
        return self

    # Repeat last n notes y times
    def repeat_old(self, steps_back: int, times: int=2) -> type(self):

        last_element_index = len(self.notes) - 1

        # Index magic shift; human brains want it one bigger
        steps_back -= 1

        for _ in range(0, times - 1):
            for i in range(last_element_index - steps_back, last_element_index + 1):
                #print("Repeating " + str(self.tones[i]) + "on index " + str(i))
                self._add_note(self.notes[i].clone())
        
        return self
    
    def repeat_section(self, section_name: str, times=1, transpose=0) -> type(self):

        if section_name in self.saved_sections:
            the_notes = self.saved_sections[section_name]
            for _ in range(0, times):
                for note in the_notes:
                    newnote = note.clone()
                    newnote.tone += transpose

                    self._add_note(newnote)

        return self

    def put(self, tone: int, dur:int=None, sus:float=None, amp:float=None) -> type(self):
        if (dur == None):
            dur = self.latest_note_data.duration
        if (sus == None):
            sus = self.latest_note_data.sustain
        if (amp == None):
            amp = self.latest_note_data.amp

        if tone != None:
            self._add_note(Note(tone, dur, sus, amp))
        else:
            self.wait(dur)
        return self

    def wait(self, duration: float) -> type(self):

        saved_note_memory = self.latest_note_data

        self._add_note(Note(None, duration))

        self.latest_note_data = saved_note_memory
        return self

    # Repeat last note x times (counting existing as 1)
    def x(self, times: int) -> type(self):
        if (times > 1):
            last_note : Note = self.notes[-1]
            tone = last_note.get_NoteData().tone
            for _ in range(0, times - 1):
                self.put(tone)    
        return self

    def alternate(self, tone_array: list, times: int, per_note:int=1, \
        dur:float=None, sus:float=None, amp:float=None) -> type(self):

        iterator = cycle(range(len(tone_array)))
        for _ in range(0, times * len(tone_array)): # Since you probably want the full sequence
            index = next(iterator)
            for _ in range(0, per_note):
                self.put(tone_array[index], dur, sus, amp)

        return self

    def add_score(self, score: Score) -> type(self):

        cloned_score : Score = score.clone()

        for note in cloned_score.notes:
            self._add_note(note)
        return self

    # A bit worn, use with caution
    def to(self, final_tone, dur_step=None, sus_step=None, amp_step=None, max_amount=-1):
        if (dur_step == None):
            dur_step = self.latest_note_data.duration
        if (sus_step == None):
            sus_step = self.latest_note_data.sustain
        if (amp_step == None):
            amp_step = self.latest_note_data.amp

        start_tone = self.notes[-1].tone

        added = 0

        while start_tone != final_tone:

            if (max_amount > 0 and (added + 1) == max_amount):
                self.put(final_tone, dur=dur_step, sus=sus_step, amp=amp_step)
                return self

            if (start_tone < final_tone):
                start_tone += 1
            else:
                start_tone -= 1
            self.put(start_tone, dur=dur_step, sus=sus_step, amp=amp_step)
            added += 1

        return self

    ### Information, private functions, etc

    def debug(self):
        totlen = 0
        for dur in self.get_durations():
            totlen += dur
        fourths = str(totlen * 4)
        log_title("Score: " + self.name)
        log("Total duration: " + str(totlen) + ", or " + fourths + "/4.")
        log("Tones: " + str(self.get_tones()))
        log("Beat String: " + self.drum_comparison_string())
        log_title("\n")
        return self

    def drum_comparison_string(self):
        stringd = ""
        for _ in range(0, int(self.dur())):
            stringd += "X."
        return stringd + " | letters: " + str(len(stringd))

    def _add_note(self, note: Note) -> None:

        self.notes.append(note)
        self._update_latest(note)

    # Re-introduction of old "add whole object non-smart"
    # Idea is you can define different "base husks" and then add them
    #   with new tones rather than always keeping track of dur/sus/amp
    def add(self, note : Note) -> Score:
        
        new_note : Note = note.clone()
        self._add_note(new_note)
        return self

    def _update_latest(self, note):
        self.latest_note_data = note.get_NoteData()
