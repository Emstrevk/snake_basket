import time
import rtmidi # This is a beauty! Cpython wrapper around c lib!
from rtmidi import MidiOut
from rtmidi.midiconstants import *

# Can't do much without pulling in everything these days
from track_lib import Track
from score_lib import Score
from note_lib import Note, NoteData

# Not sure how to wrap but the lib examples have a wrapper class
# Most important is:
# 1. Note is sent on channel with velocity, will be noteON and noteOFF
# 2. DUR determines when the next NoteOn will happen, SUS determines noteOFF
# 3. It is probably best to first read all tracks and create a unified list of midi notes with times
# 4. Then you start ticking by time, triggering events as they happen

'''
    Prerequisites:
        1. All MidiNotes in list sorted by start_time
        2. start_time determined from accumulated dur stats when working through the tracks
    LOOOP:
    each <timeUnit> seconds
        <timeUnit> -> <currentTime>
        check started array : each entry a counter that reduces by <timeUnit> (or an endtime for currentTime comp)
            noteOff sent if end_time equals <currentTime> or timer is empty, depending on method
        check next MidiNote : if start_time equals <currentTime> send noteOn
'''

class MidiNote:
    
    def __init__(self, start_beat, end_beat, channel, velocity, tone):
        self.start_time = start_beat
        self.end_time = end_beat
        self.channel = channel
        self.velocity = velocity
        self.tone = tone

    # Sortable by time
    # TODO: Should probably have a fallback for non-score even if that should never happen
    def __lt__(self, other):
        cast_to_midi : MidiScore = other
        return self.start_time < cast_to_midi.start_time

class MidiScore:

    def __init__(self, notes = []):
        self.midi_notes = notes

    # Duplicate code, only differs in what attribute to check
    def pop_at_end_beat(self, beat : float) -> list:

        popped = []

        index = 0
        while index < len(self.midi_notes):
            search : MidiNote = self.midi_notes[index]

            if search.end_time == beat:
                popped.append(search)
            else:
                # Skip to end since there are no more notes starting
                # at this time
                index = len(self.midi_notes)

        for note in popped:
            self.midi_notes.remove(note)

        return popped

    def pop_at_start_beat(self, beat : float) -> list:

        popped = []

        index = 0
        while index < len(self.midi_notes):
            search : MidiNote = self.midi_notes[index]

            if search.start_time == beat:
                popped.append(search)
            else:
                # Skip to end since there are no more notes starting
                # at this time
                index = len(self.midi_notes)

        for note in popped:
            self.midi_notes.remove(note)

        return popped


    # Converts a tracklib track into usable midi convention data
    # Tracks operate on a dur/sus basis while midi is more concerned with raw time
    def add_track(self, track : Track, channel : int) -> None:
        track_score : Score = track.full_score

        #track_channel : int = track.instrument # TODO: Bad naming, bad dependency

        # Time data saved as "beats" which is resolved to "real time" using BPM in playback
        beat : float = 0.0      

        for note in track_score.notes:
            # Cast and assign
            cast_note : Note = note
            note_data : NoteData = cast_note.get_NoteData()

            # Simple velocity logic, could be smarter
            # This effectively makes 1.0 the highest possible amp
            velocity = note_data.amp * 255
            if velocity > 255: velocity = 255

            new_midi : MidiNote = MidiNote(
                beat,
                beat + note_data.sustain,
                channel,
                velocity,
                note_data.tone
            )

            # Add the time "reserved" by the note
            beat += note_data.duration

            # Add note with current time data, move on to next...
            self.midi_notes.append(new_midi)
        
class MidiPlayer:

    def __init__(self, bpm : int):
        self._bpm = bpm

    def play(self, score : MidiScore):

        midiout : MidiOut = MidiOut()

        started_notes : MidiScore = MidiScore()
        
        current_beat : float = 0.0
        
        final_note : MidiNote = score.midi_notes[-1]

        end_beat = final_note.end_time

        while current_beat <= end_beat:
            
            # Find notes starting at the current time
            
            if (len(score.midi_notes) > 0):

                # Extract all notes starting at this time
                # This will remove them from the score 
                notes_starting_here = score.pop_at_start_beat(current_beat)

                for search in notes_starting_here:

                    # Stolen from example; the first byte contains both NOTE_ON and
                    #   the channel to note_on - as in 0x9<ch>
                    #   This performs the append
                    msg = [(NOTE_ON & 0xF0) | (search.channel & 0xF)]
                    # Note: Order is important, msg is a byte array midi message following general spec
                    msg.append(search.tone)
                    msg.append(search.velocity)

                    midiout.send_message(msg)

                    started_notes.midi_notes.append(search)

            # Perform a similar operation for noteOFF events
            if (len(started_notes.midi_notes) > 0):
                notes_ending_here = started_notes.pop_at_start_beat(current_beat)

                for search in notes_ending_here:
                    msg = [(NOTE_OFF & 0xF0) | (search.channel & 0xF)]
                    # Note: Order is important, msg is a byte array midi message following general spec
                    msg.append(search.tone)
                    msg.append(search.velocity)

                    midiout.send_message(msg)


            # This is where bpm comes in
            # Not super sure how to work it but say we increase by one beat
            time.sleep(self.to_seconds(1.0))
            current_beat += 1.0
    
    # I think this is correct
    def to_seconds(self, midi_beats : float):
        bpm = self._bpm
        bps = bpm / 60
        
        return midi_beats / bps
