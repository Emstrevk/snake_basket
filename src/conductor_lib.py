# Python 3.7 legacy, can be removed in 4.0
from __future__ import annotations

#from FoxDot import Clock, MidiOut, Player, play, Scale, keys
from track_lib import Track
from score_lib import Score

# TODO: Temp methods
def log(message):
    print(message)

def log_title(message):
    print(message)

# Class for unified track functionality; the conductor of the full song
class Conductor:

    def __init__(self, bpm=120):
        self.bpm = bpm
        self.tracks = []
        log("BPM: " + str(bpm))

        self.timed_sections = {}

    # Similar to putting the cursor here in a GUI editor
    # e.g. start playing here, ignoring everything before
    def CURSOR_HERE(self) -> type(self):
        for track in self.tracks:
            track.mute_until_here()
        return self

    # Mute all tracks not in this list
    # Only works if done last in spec
    def solo(self, solo_tracks:list) -> type(self):
        for track in self.tracks:
            if track not in solo_tracks:
                track.mute_until_here()
        return self 

    # Similar to
    def mute(self, muted_tracks:list) -> type(self):
        for track in muted_tracks:
            track.mute_until_here()
        return self 

    def info(self):
        
        i = 0
        log_title("Track info:")
        for track in self.tracks:
            i += 1
            log(str(i) + ". " + track.name + ": " + track.track_info())
        log_title("\n")

    # Keep in sync with Track.__init__()
    # Exists to minimize boilerplate of adding tracks to the master
    def new(self, instrument=0):
        track = Track(instrument)
        self.tracks.append(track)
        return track

    # Make sure all tracks have the same length by padding shorter ones with silence
    def sync(self, excluded=[]):
        longest_track = self._get_longest_track()

        # Start padding
        for track in self.tracks:
            if track not in excluded:
                track.wait_until_now(longest_track)

        return self

    def _get_longest_track(self):
        longest_track = None
        highest_dur = 0
        # Get the longest duration
        for track in self.tracks:
            if (track.dur() > highest_dur):
                longest_track = track
                highest_dur = track.dur()

        return longest_track

    def lead_score(self, lead_score):
        for track in self.tracks:
            track.along(lead_score)
                
        return self

    def play(self, track_list, lead_score : Score, times=1):
        for track in track_list:
            track.along(lead_score).play(times)
        return self

    def reverse_all(self):
        for track in self.tracks:
            print("Reversing track")
            track.reverse()

    # Wait with all tracks (complete silence)
    def wait(self, units, exclude=[]):
        for track in self.tracks:
            if track not in exclude:
                track.wait(units)
        return self