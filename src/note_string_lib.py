from tone_lib import char_map
from note_lib import Note, NoteData

OPEN = "["
CLOSE = "]"
SUSTAIN = "-"
QUIET = "."
SPECIALS = [OPEN, CLOSE, SUSTAIN, QUIET]

# Keeps track of notes added within brackets
# Will adjust these objects directly
class BracketedSection:

    def __init__(self, base_dur, sustain_multiplier):
        self.data_array = []
        self.base_dur = base_dur
        self.sustain_multiplier = sustain_multiplier

    def append(self, note_data:NoteData) -> None:
        self.data_array.append(note_data)

        total_notes = len(self.data_array)
        each_allowed = self.base_dur / total_notes

        for data in self.data_array:
            data.duration = each_allowed
            data.adjust_sustain(self.sustain_multiplier)

class NoteParser:

    bracketed_section = None

    def __init__(self, note_string, amp, base_dur, sus_multiplier):

        self.base_dur = base_dur
        self.sus_multiplier = sus_multiplier
        self.data_array = []
        self.amp = amp
        self.note_list = []

        self.action_map = {}
        self.action_map[OPEN] = self._open_brackets
        self.action_map[CLOSE] = self._close_brackets
        self.action_map[SUSTAIN] = self._sustain_last
        self.action_map[QUIET] = self._add_silence

        # Populate note data 
        for char in note_string:
            self._parse_char(char)

        for note_data in self.data_array:
            self.note_list.append(Note.from_NoteData(note_data))

        print(note_string + " parsed ok!")

    def _parse_char(self, char):

        if char in SPECIALS:
            self.action_map[char]()
        else:
            self._parse_note(char)
            
    def _parse_note(self, char):
        index = char_map[char]
        data = NoteData(index, self.base_dur, None, self.amp)
        data.adjust_sustain(self.sus_multiplier)
        self.data_array.append(data)

        if self.bracketed_section != None:
            self.bracketed_section.append(data)

    def _open_brackets(self):
        if self.bracketed_section != None:
            print("NOTE_STRING_ERROR: Attempted to open brackets twice")
            return
        
        self.bracketed_section = BracketedSection(self.base_dur, self.sus_multiplier)

    def _close_brackets(self):
        if self.bracketed_section == None:
            print("NOTE_STRING_ERROR: Attempted to close non-open brackets")
            return

        self.bracketed_section = None

    # Add a base dur unit to last note and set sustain accordingly
    def _sustain_last(self):
        
        if self.data_array[-1] == None:
            print("NOTE_STRING_ERROR: Attempted to sustain (-) with no prior note")
            return 

        last_note_data = self.data_array[-1]
        last_note_data.duration += self.base_dur
        last_note_data.adjust_sustain(self.sus_multiplier)

    def _add_silence(self):
        if self.bracketed_section != None:
            print("NOTE_STRING_WARNING: Breaks in brackets not implemented")

        # Create silent notedata
        data = NoteData(0, self.base_dur, None, 0)
        data.adjust_sustain(self.sus_multiplier)
        data.silence()

        self.data_array.append(data)
