from pysndfx import AudioEffectsChain
from os import path


def _get_ouput_file(input_filepath) -> str:
    file_name_extension_split = path.basename(input_filepath).split(".")
    file_dir = path.dirname(input_filepath)
    file_name = file_name_extension_split[0]
    extension = file_name_extension_split[1]

    return file_dir + "/" + file_name + "_processed." + extension

def reverb(input_filepath) -> str:
    print("Applying reverb to " + input_filepath)

    output_name = _get_ouput_file(input_filepath)

    print("Reverb file:" + output_name)
    AudioEffectsChain().reverb(reverberance=70, room_scale=100)(input_filepath, output_name)

    return output_name