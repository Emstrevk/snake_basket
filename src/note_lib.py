import math
import re
import random

# Data holder, can hold null values and used for such scenarios
class NoteData:

    def clone(self):
        return NoteData(self.tone, self.duration, self.sustain, self.amp)

    def __init__(self, tone=None, dur=None, sus=None, amp=None):
        self.tone = tone 
        self.duration = dur 
        self.sustain = sus
        self.amp = amp

    def silence(self):
        self.tone = 1
        self.amp = 0

    def adjust_sustain(self, dur_multiplier):
        if self.duration is not None:
            self.sustain = self.duration * dur_multiplier

class Note:

    @classmethod
    def from_NoteData(self, note_data : NoteData):
        new_note = Note()
        new_note._data = note_data.clone()
        return new_note

    # Attempted "Use different tone" syntax
    def __call__(self, override_tone):
        clone = self.clone()
        clone.get_NoteData().tone = override_tone
        return clone

    def __init__(self, tone=None, dur=1, sus=1, amp=1):
        self._data = NoteData(tone, dur, sus, amp)

        # None-tone shorthand for silences
        if (tone == None):
            self._data.silence()

    # Create a list of repetitions; space-efficient when adding many of same to score
    def repeat(self, size):
        repeats = []
        for i in range(0, size):
            repeats.append(self.clone())
        return repeats

    def clone(self):
        return Note.from_NoteData(self._data)

    def get_NoteData(self):
        return self._data


