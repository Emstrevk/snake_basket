from midiutil import MIDIFile
from track_lib import Track
from note_lib import Note, NoteData
from random import randint
from tone_lib import *

class MIDIConverter:

    # https://github.com/maxmcorp/mxm.midifile/blob/master/mxm/midifile/examples/mxm_midifile_type_1_8ch_8trk_random.py
    # Not sure how it all works

    OCTAVE_CORRECTION = OCTAVE * 4

    def __init__(self, bpm=120):
        self.bpm = bpm

    def _midi_velocity(self, amp_val):
        max_value = 255
        base_value = max_value 

        target = (base_value / 2) * amp_val

        if (target > max_value):
            target = max_value
        elif target < 0:
            target = 0

        return int(target)

    def _midi_tone(self, tone_index):
        return tone_index + self.OCTAVE_CORRECTION

    def _to_midi_time(self, seconds):
        #return int(self.WHOLE_NOTE_LENGTH * seconds)

        bpm = self.bpm
        bps = bpm / 60 # 60 is seconds in a minute
        length_in_beats = seconds * bps

        # TUrns out that if you specify bpm in the beat track
        #   you don't have to calculate manually; it uses the same beats unit
        #   as the one from foxdot that you've taken to calling "seconds" or "units"
        return seconds

    def _write_track(self, midi_file: MIDIFile, track:Track, track_index:int):
        current_time = 0

        # Program change events typically specify instrument
        # Actually channel seems to be it but I'm keeping this as cargo cult
        # https://stackoverflow.com/questions/23564706/no-preset-found-on-channel-9-when-playing-midi-with-newly-created-soundfont
        channel = track_index # Seems channel + instrument is some kind of bank

        # This works alone to some degree, seems to get muddled if you do below too
        # Then again nothing changes on instrument along... channel switch req
        midi_file.addProgramChange(track_index, channel, 0, track.instrument)
 #       midi_file.changeTuningBank(track_index, channel, 1, track.instrument) # Complete hoobojoobo
#        midi_file.changeTuningProgram(track_index, channel, 2, track.instrument)

        old_time = current_time
        for foxnote in track.full_score.notes:

            data: NoteData = foxnote.get_NoteData()

            velocity = self._midi_velocity(data.amp)
            tone = self._midi_tone(data.tone)
            duration = self._to_midi_time(data.duration)
            sustain = self._to_midi_time(data.sustain)

            # TEsting
            track_no = track_index
            #track_no = channel
            
            if data.amp > 0:

                if (old_time > 0):
                    midi_file.addChannelPressure(track_no, channel, current_time, self._get_pressure(data.amp))
                    midi_file.addPitchWheelEvent(track_no, channel, old_time, tone) # Somewhat organic??
                midi_file.addNote(track_no, channel, tone, current_time, sustain, velocity)
            else:
                midi_file.addNote(track_no, channel, tone, current_time, sustain, 0) 

            current_time = current_time + duration

    # Experimental
    def _get_pressure(self, amp_value) -> int:
        base = amp_value * 127
        if base > 127:
            return 127
        else:
            return int(base)

    def create_midi_file(self, foxlib_tracks, file_name="newMidi.mid") -> str:

        print("Creating MIDI file: " + file_name)

        filtered_tracks = []

        for track in foxlib_tracks:
            # Skip drums for now
            if (isinstance(track, Track) and track.full_score.notes is not None and len(track.full_score.notes) > 0):
                filtered_tracks.append(track)
        
        track_index = 0
        current_time = 0
        bpm_tempo = self.bpm
        midi_file = MIDIFile(len(filtered_tracks) + 1)  # Tempo track will take one place
        midi_file.addTempo(track_index, current_time, bpm_tempo)
        track_index += 1

        for track in filtered_tracks:
            self._write_track(midi_file, track, track_index)
            track_index += 1
        
        with open(file_name, "wb") as output_file:
            midi_file.writeFile(output_file)

            return file_name