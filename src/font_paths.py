
# Constants for font paths
# Fonts are not included in git repo and will probably
#   be missing.

SEVENTIES = "soundfonts/70s.sf2"
ROLAND_BRIGHT = "soundfonts/R-bright.sf2"
MOOG = "soundfonts/Moog1.sf2"
SPACE_SWEEP = "soundfonts/Spacesweep.sf2"
VIRAL_DRUMS = "soundfonts/JJ_VIRAL_MASSACRE.sf2"
BORCH_BATTERY = "soundfonts/BorchBattery.sf2"
REGAL = "soundfonts/Regalsyn.sf2"
POLY = "soundfonts/Polysyn.sf2"
NINTENDO = "soundfonts/nintendo_soundfont.sf2"
CHIP = "soundfonts/CPS2SFV1.0.sf2"
VIBRASAW = "soundfonts/Vibrasaw.sf2"
FL_SAW = "soundfonts/FLStudio/Super Saw 2.sf2"
FL_SOLAR = "soundfonts/FLStudio/Solar Wind.sf2"