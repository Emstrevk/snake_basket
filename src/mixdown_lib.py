from midiutil import MIDIFile
from track_lib import Track
from note_lib import Note, NoteData
from random import randint
from fluidsynth_lib import AlsaFluidSynth
from pydub import AudioSegment
from pydub.playback import play
from midi_format_lib import MIDIConverter

MIXDOWN_DIR = "mixdowns/"

# Usage: Add tracks with given soundfonts, call mix to make
#   a combined mp3 of all tracks
class Mixdown:

    def __init__(self):
        self.midiMixes = []
    
    def add(self, tracks : list, fontpath : str, name: str):
        self.midiMixes.append(MidiMix(tracks, fontpath, name))

    def mix(self, output_file : str) -> str:
        print("Starting mixdown")
        fs = AlsaFluidSynth()
        wav_files = []
        for midi_mix in self.midiMixes:
            wav_files.append(midi_mix.to_wav(fs))
        
        # "With-last" merge of all files
        # pydub -> combine all mp3 files
        last_merge = None
        for wav_file in wav_files:
            print("Merging...")
            sound = AudioSegment.from_file(wav_file)
            if (last_merge != None):
                last_merge = sound.overlay(last_merge)
            else:
                last_merge = sound

        # TODO: This can also add metadata
        last_merge.export(MIXDOWN_DIR + output_file, format='wav')

        return MIXDOWN_DIR + output_file

    def play(self, wav_path : str):
        audio = AudioSegment.from_wav(wav_path)
        play(audio)
        
        # Or other from https://stackoverflow.com/questions/7629873/how-do-i-mix-audio-files-using-python
        # Cleanup or suchlike

class MidiMix:

    def __init__(self, tracks : list, fontpath : str, name : str):
        self.tracks = tracks
        self.fontpath = fontpath
        self.name = name

    # Make single track mp3 and return its path
    def to_wav(self, fs:AlsaFluidSynth) -> str:
        print("#### TO_WAV: Set sound-font")
        fs.sound_font = self.fontpath
        print("#### TO_WAV: Create MIDI converter")
        midic = MIDIConverter(bpm=120)
        print("#### TO_WAV: create_midi_file")
        midi_file = midic.create_midi_file(self.tracks, MIXDOWN_DIR + self.name + ".mid")
        print("#### TO_WAV: FS MIDI_TO_AUDIO")
        fs.midi_to_audio(midi_file, MIXDOWN_DIR + self.name + ".wav")

        return MIXDOWN_DIR + self.name + ".wav"