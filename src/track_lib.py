# Python 3.7 legacy, can be removed in 4.0
from __future__ import annotations

from score_lib import Score
from note_lib import Note
import re 

# Fill in for missing old import
def log(message):
    print(message)

# Wrapper for a score and a set of quiet beats
# TODO: In dire need of documentation
class Track:

    def __init__(self, instrument=0):
        self.full_score = Score()
        self.name = "todo"
        self.instrument = instrument
        self.lead_score = None
        self.melody_score = Score()

    def mute_until_here(self) -> None:
        self.full_score.wipe()

    def score(self) -> Score:
        return self.melody_score

    # Inject a one-shot note string at track level, e.g. for dynamic drums and solos
    # Can play just as stated or repeatedly with a lead
    def dynamic(self, note_string:str, lead_score:Score=None, transpose=0, amp=1, sus=0.9, times:int=1) -> Track:

        only_once = (lead_score == None)

        one_shot_score = Score().parse(note_string, amp=amp, sus_x=sus)
        one_shot_score.transpose_all(transpose)

        # Play along lead or just make a single one
        if not only_once:
            repetition_score = Score()
            
            for _ in range(0, times):
                while repetition_score.dur() < lead_score.dur():
                    #print("DEBUG: Padding dur: " + str(repetition_score.dur()))
                    repetition_score.add_score(one_shot_score)

                self.full_score.add_score(repetition_score)
        else:
            self.full_score.add_score(one_shot_score)

        return self

    # THis might be ripe for removal
    def beat_time(self, time_in_seconds:float):
        self.beat_len = time_in_seconds
        return self


    # TODO: At the end of the day the unit for "unit" becomes problematic
    # As of now drum samples are always 1/2 unit, meaning they have to be 
    # even to be matchable with all these (unit) functions
    # Ideally of course one should be able to wait(0.5)
    def wait(self, units:float, idle_tone=None) -> type(self):
        self.full_score.wait(units)
    
        return self

    def wait_out(self, other_score:Score, times=1) -> type(self):
        time_to_play = other_score.dur() * times
        return self.wait(time_to_play)

    def wait_until_now(self, up_to_date_track:Track, idle_tone=None) -> type(self):
        current = self.dur()
        goal = up_to_date_track.dur()

        # Possible bug source if you start at an uneven place
        padding = int(goal - current)
        return self.wait(padding, idle_tone)

    # Repeatedly play for the duration of other score
    def play_along(self, other_score:Score, times=1) -> type(self):
        time_to_play = int(other_score.dur()) * times
        print("DEBUG: Received a play-along of " + str(time_to_play) + "for own dur " + str(self.melody_dur()))
        return self.play_for_units(time_to_play)

    # Play once, then wait for other score to finish
    def _play_then_pad(self, other_score:Score, times:int=1) -> type(self):
        total_time = other_score.dur()
        own_time = self.melody_dur()
        padding_time = total_time - own_time

        for _ in range(0, times):
            self.play(1)
            self.wait(padding_time)

        return self

    # Play as many times as you have to to match units
    # Currently problematic for both too long and uneven melodies
    def play_for_units(self, units:float) -> type(self):

        additions = Score()
        while additions.dur() < units:
            additions.add_score(self.melody_score)

        self.full_score.add_score(additions)
        return self

    # Effectively turns "play()" into "play_along()" to allow easier typing for supporting tracks
    # TODO: Also modify wait() into wait_out() ?
    def along(self, score:Score) -> type(self):
        self.lead_score = score
        return self

    # opl = "Once Per Lead"; difference between inf loop and waiting out
    def play(self, times:int=1, opl=False) -> type(self):

        # See along()
        if self.lead_score != None:
            if opl:
                return self._play_then_pad(self.lead_score, times)
            else:
                return self.play_along(self.lead_score, times)

        # Default functionality; loop self x times
        return self.play_once(times)

    # Alternative play() if lead is defined
    def play_once(self, times:int=1) -> type(self):
        for _ in range(0, times):
            self.full_score.add_score(self.melody_score)
        return self

    def reverse(self):
        self.full_score.reverse()
        return self

    def humanize(self):
        self.full_score.humanize()
        return self

    # Full score dur, technically easy to fetch manually but more uniform this way
    def dur(self) -> float:
        return self.full_score.dur()

    def melody_dur(self) -> float:
        return self.melody_score.dur()

    def track_info(self) -> str:
        return str(self.melody_dur()) + " | " + str(self.dur())