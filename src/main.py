from score_lib import Score
from track_lib import Track
from note_lib import Note
from conductor_lib import Conductor
from midi_format_lib import MIDIConverter
from mixdown_lib import Mixdown
from tone_lib import *
from font_paths import *
from effects_lib import *
import sys


# Setup midi file prerequisites
#midi_filename = sys.argv[1]
midic = MIDIConverter(bpm=120)

# Init conductor

tracks = Conductor(bpm=120)

############################### Define

bass = tracks.new(0)
b1 : Note = Note(C, 1/4, 1/8, 1)
b2 : Note = Note(C, 1/8, 1/16, 0.8)

bass.score().add(b1).wait(1/4).add(b2).x(2).add(b1(C2))
bass.score().add(b1).wait(1/4).add(b2).x(2).add(b1(C2))
bass.score().add(b1).wait(1/4).add(b2).x(2).add(b1(C2))
bass.score().add(b1).wait(1/4).add(b2).x(2).add(b1(C2))
bass.score().add(b1(Db2)).wait(1/4).add(b2).x(2).add(b1(C2))
bass.score().add(b1(Db2)).wait(1/4).add(b2).x(2).add(b1(C2))
bass.score().add(b1).wait(1/4).add(b2).x(2).add(b1(C2))
bass.score().add(b1).wait(1/4).add(b2).x(2).add(b1(C2))

drums = tracks.new(0)

riff = tracks.new(0)
r1 : Note = Note(C, 1/2, 1/2, 0.7)
riff.score().add(r1(D2)).add(r1(E2)).wait(1/2).add(r1(C2))
riff.score().add(r1(D2)).add(r1(E2)).wait(1/2).add(r1(C2))
riff.score().add(r1(F2)).add(r1(E2)).wait(1/2).add(r1(C2))
riff.score().add(r1(D2)).add(r1(E2)).wait(1/2).add(r1(C2))

organ = tracks.new()
o1 : Note = Note(C, 4, 4, 0.5)

organ.score().add(o1).add(o1(D))
organ.score().add(o1(G)).add(o1(E)).transpose_all(-OCTAVE)

riff2 = tracks.new()
ri1 : Note = Note(C, 1/4, 1/4, 1)
ri2 : Note = Note(C, 1/4, 1/8, 0.8)

riff2.score().parse("C--DE--D", amp=0.4)
riff2.score().parse("E--FD--D", amp=0.4)
riff2.score().parse("C--DC--C", amp=0.4)
riff2.score().parse("C--DE--E", amp=0.4)
riff2.score().transpose_all(-OCTAVE)

############################## Compose

tracks.wait(1)
tracks.play([bass, riff, riff2, organ], bass.score())


tracks.wait(1)
drums.dynamic("C...C..." + ".G.D....", bass.score(), times=4)
tracks.play([bass], bass.score(), 4).sync()

drums.dynamic("[CC]C..C..." + "[CC]..D....", bass.score(), times=4)
tracks.play([bass, riff], bass.score(), 4).sync()

drums.dynamic("d")
tracks.sync()

drums.dynamic("[CC]C.E.F..", bass.score(), times=4)
tracks.play([bass], bass.score(), 4).sync()

drums.dynamic("[CC]C.b", bass.score(), times=4)
tracks.play([bass, riff2, organ], bass.score(), 4).sync()

drums.dynamic("CCC")
tracks.sync()

drums.dynamic("C...", bass.score(), times=2)
tracks.play([bass], bass.score(), 2).sync()

drums.dynamic("C.cC", bass.score(), times=4)
tracks.play([bass, riff2, organ], bass.score(), 4).sync()

drums.dynamic("G")
tracks.sync()

drums.dynamic("[CC]C[CC]c", bass.score(), 4)
tracks.play([bass, riff], bass.score(), 4).sync()

drums.dynamic("D")
tracks.sync()

tracks.wait(1)

#tracks.reverse_all()

print("Attempting mixdown..")

mix = Mixdown()
mix.add([bass], CHIP, "bass")
mix.add([drums], BORCH_BATTERY, "track1")
mix.add([riff], FL_SAW, "riff")
mix.add([organ], SEVENTIES, "organ")
mix.add([riff2], ROLAND_BRIGHT, "riff2")
final_mix_file = mix.mix("test_mix.wav")


final_mix_file = reverb(final_mix_file)

mix.play(final_mix_file)

# Save the finished midi_file
#midic.create_midi_file(tracks.tracks, midi_filename)


